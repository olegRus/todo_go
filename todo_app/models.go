package todoapp

import (
	"time"

	"github.com/google/uuid"
)

type ToDoRequestModel struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	IsCompleted bool   `json:"is_complete"`
}

type ToDoApiModel struct {
	Id          uuid.UUID
	Title       string
	Description string
	IsCompleted bool
	CreatedAt   time.Time
}

type ToDoStoreModel struct {
	id          uuid.UUID
	title       string
	description string
	isCompleted bool
	createdAt   time.Time
	isDeleted   bool
}
