package todoapp

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func Configure() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config/")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("cant read config")
		os.Exit(1)
	}
}
