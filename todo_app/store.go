package todoapp

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/viper"
)

var pool pgxpool.Pool

type NotFoundError struct {
	Msg string
}

func (e *NotFoundError) Error() string {
	return e.Msg
}

func InitDbConnection() {
	connectionUrl := fmt.Sprintf(
		"postgresql://%v:%v@%v/%v",
		viper.GetString("db_user"),
		viper.GetString("db_password"),
		viper.GetString("db_url"),
		viper.GetString("db_name"))
	log.Default().Println(connectionUrl)
	init_pool, err := pgxpool.Connect(context.Background(), connectionUrl)
	if err != nil {
		log.Fatalf("Unable to connection to database: %v\n", err)
		os.Exit(1)
	}
	pool = *init_pool
}

func CloseDbConnection() {
	log.Default().Println("db connection closed")
	pool.Close()
}

func saveToDo(requestTodo *ToDoRequestModel) (*ToDoApiModel, error) {
	var storeToDo = MapRequestModelToStoreModel(requestTodo)
	storeToDo.createdAt = time.Now().UTC()

	err := saveToDoToDb(&storeToDo)
	if err != nil {
		return nil, err
	}

	//c4, f5, da, 34, 28, 6f, 49, 27, b5, e9, 3a, 2c, d9, 22, 51, 04
	apiTodo := MapStoreModelApiModel(&storeToDo)
	log.Default().Println(apiTodo)

	return &apiTodo, nil
}

func readToDo(id uuid.UUID) (*ToDoApiModel, error) {
	storeTodo, err := readToDoFromDb(id)
	if err != nil {
		return nil, err
	}

	apiTodo := MapStoreModelApiModel(storeTodo)
	return &apiTodo, nil
}

func deleteToDo(id uuid.UUID) error {
	storeTodo, err := readToDoFromDb(id)
	if err != nil {
		return err
	}

	storeTodo.isDeleted = true
	err = saveToDoToDb(storeTodo)

	return err
}

func updateToDo(id uuid.UUID, todo *ToDoRequestModel) (*ToDoApiModel, error) {
	storedTodo, err := readToDoFromDb(id)
	if err != nil {
		return nil, err
	}

	updatedTodo := MapRequestModelToStoreModel(todo)
	updatedTodo.id = id
	updatedTodo.createdAt = storedTodo.createdAt

	err = saveToDoToDb(&updatedTodo)
	if err != nil {
		return nil, err
	}

	apiTodo := MapStoreModelApiModel(&updatedTodo)
	return &apiTodo, nil
}

func readToDoFromDb(id uuid.UUID) (*ToDoStoreModel, error) {
	connection, err := pool.Acquire(context.Background())
	if err != nil {
		return nil, err
	}

	row := connection.QueryRow(context.Background(),
		`SELECT id, title, description, iscompleated, createdat, isdeleted FROM public.todos
		where id = $1;`, id.String())

	var storeTodo ToDoStoreModel
	err = row.Scan(&storeTodo.id,
		&storeTodo.title,
		&storeTodo.description,
		&storeTodo.isCompleted,
		&storeTodo.createdAt,
		&storeTodo.isDeleted)
	if err != nil {
		return nil, err
	}
	if storeTodo.isDeleted {
		return nil, &NotFoundError{fmt.Sprintf("todo '%v' not found", id.String())}
	}
	return &storeTodo, nil
}

func saveToDoToDb(todo *ToDoStoreModel) error {
	connection, err := pool.Acquire(context.Background())
	if err != nil {
		return err
	}
	defer connection.Release()

	row := connection.QueryRow(context.Background(),
		`INSERT INTO public.todos
		(id, title, description, isCompleated, createdAt, isDeleted)
			VALUES($1, $2, $3, $4, $5, $6)
		ON CONFLICT (id) DO UPDATE
			SET title=$2, description=$3, isCompleated=$4, createdAt=$5, isDeleted=$6
		RETURNING id;
		`, todo.id.String(), todo.title, todo.description, todo.isCompleted, todo.createdAt.Format("2006-01-02 15:04:05"), todo.isDeleted)

	var id_str string
	err = row.Scan(&id_str)
	if err != nil {
		return err
	}
	return nil
}
