package todoapp

import (
	"github.com/google/uuid"
)

func MapRequestModelToStoreModel(requestModel *ToDoRequestModel) ToDoStoreModel {
	var storeTodo ToDoStoreModel

	storeTodo.id = uuid.New()
	storeTodo.title = requestModel.Title
	storeTodo.description = requestModel.Description
	storeTodo.isCompleted = requestModel.IsCompleted

	storeTodo.isDeleted = false

	return storeTodo
}

func MapApiModel(apiModel *ToDoApiModel) ToDoStoreModel {
	var storeToDo ToDoStoreModel
	storeToDo.id = apiModel.Id
	storeToDo.title = apiModel.Title
	storeToDo.description = apiModel.Description
	storeToDo.isCompleted = apiModel.IsCompleted
	storeToDo.createdAt = apiModel.CreatedAt
	storeToDo.isDeleted = false
	return storeToDo
}

func MapStoreModelApiModel(storeModel *ToDoStoreModel) ToDoApiModel {
	var apiToDo ToDoApiModel
	apiToDo.Id = storeModel.id
	apiToDo.Title = storeModel.title
	apiToDo.Description = storeModel.description
	apiToDo.IsCompleted = storeModel.isCompleted
	apiToDo.CreatedAt = storeModel.createdAt
	return apiToDo
}
