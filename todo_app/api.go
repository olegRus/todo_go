package todoapp

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

func ToDoCreate(w http.ResponseWriter, r *http.Request) {
	var newTodo ToDoRequestModel
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	json.Unmarshal(reqBody, &newTodo)
	createdToDo, err := saveToDo(&newTodo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(createdToDo)
}

func ToDoRead(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err.Error())
	}

	todo, err := readToDo(id)
	if err != nil {
		var notFoundErr *NotFoundError
		switch {
		case errors.As(err, &notFoundErr):
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		default:
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		}
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(todo)
	}
}

func ToDoUpdate(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err.Error())
	}

	var requestTodo ToDoRequestModel
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}

	json.Unmarshal(reqBody, &requestTodo)

	updatedToDo, err := updateToDo(id, &requestTodo)
	if err != nil {
		var notFoundErr *NotFoundError
		switch {
		case errors.As(err, &notFoundErr):
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		default:
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		}
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(updatedToDo)
	}
}

func ToDoDelete(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err.Error())
	}

	err = deleteToDo(id)
	if err != nil {
		var notFoundErr *NotFoundError
		switch {
		case errors.As(err, &notFoundErr):
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		default:
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintln(w, err.Error())
		}
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

func getId(r *http.Request) (uuid.UUID, error) {
	id_str := mux.Vars(r)["id"]
	var id uuid.UUID
	err := id.Scan(id_str)
	return id, err
}
