package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	todoapp "gitlab.com/olegRus/todo_go/todo_app"
)

func main() {
	todoapp.Configure()
	todoapp.InitDbConnection()
	defer todoapp.CloseDbConnection()

	router := mux.NewRouter()
	router.HandleFunc("/api/todo", todoapp.ToDoCreate).Methods("POST")
	router.HandleFunc("/api/todo/{id}", todoapp.ToDoRead).Methods("GET")
	router.HandleFunc("/api/todo/{id}", todoapp.ToDoDelete).Methods("DELETE")
	router.HandleFunc("/api/todo/{id}", todoapp.ToDoUpdate).Methods("PUT")

	log.Default().Println(http.ListenAndServe(":9000", router))
}
